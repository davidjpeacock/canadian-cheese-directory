package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type Cheese struct {
	CheeseId             string
	CheeseNameEn         string
	CheeseNameFr         string
	ManufacturerNameEn   string
	ManufacturerNameFr   string
	ManufacturerProvCode string
	ManufacturingTypeEn  string
	ManufacturingTypeFr  string
	WebSiteEn            string
	WebSiteFr            string
	FatContentPercent    string
	MoisturePercent      string
	ParticularitiesEn    string
	ParticularitiesFr    string
	FlavourEn            string
	FlavourFr            string
	CharacteristicsEn    string
	CharacteristicsFr    string
	RipeningEn           string
	RipeningFr           string
	Organic              string
	CategoryTypeEn       string
	CategoryTypeFr       string
	MilkTypeEn           string
	MilkTypeFr           string
	MilkTreatmentTypeEn  string
	MilkTreatmentTypeFr  string
	RindTypeEn           string
	RindTypeFr           string
	LastUpdateDate       string
}

func main() {

	fmt.Println("Fetching Canadian Cheese Directory from Canada Open Data Portal...")

	// Canada Open Data Portal; Canadian Cheese Directory CSV file
	ccdUrl := "http://cheese-fromage.agr.gc.ca/od-do/canadianCheeseDirectory.csv"

	resp, err := http.Get(ccdUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	reader := csv.NewReader(resp.Body)
	reader.FieldsPerRecord = -1

	csvData, err := reader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	var oneRecord Cheese
	var allRecords []Cheese

	for _, each := range csvData {
		oneRecord.CheeseId = each[0]
		oneRecord.CheeseNameEn = each[1]
		oneRecord.CheeseNameFr = each[2]
		oneRecord.ManufacturerNameEn = each[3]
		oneRecord.ManufacturerNameFr = each[4]
		oneRecord.ManufacturerProvCode = each[5]
		oneRecord.ManufacturingTypeEn = each[6]
		oneRecord.ManufacturingTypeFr = each[7]
		oneRecord.WebSiteEn = each[8]
		oneRecord.WebSiteFr = each[9]
		oneRecord.FatContentPercent = each[10]
		oneRecord.MoisturePercent = each[11]
		oneRecord.ParticularitiesEn = each[12]
		oneRecord.ParticularitiesFr = each[13]
		oneRecord.FlavourEn = each[14]
		oneRecord.FlavourFr = each[15]
		oneRecord.CharacteristicsEn = each[16]
		oneRecord.CharacteristicsFr = each[17]
		oneRecord.RipeningEn = each[18]
		oneRecord.RipeningFr = each[19]
		oneRecord.Organic = each[20]
		oneRecord.CategoryTypeEn = each[21]
		oneRecord.CategoryTypeFr = each[22]
		oneRecord.MilkTypeEn = each[23]
		oneRecord.MilkTypeFr = each[24]
		oneRecord.MilkTreatmentTypeEn = each[25]
		oneRecord.MilkTreatmentTypeFr = each[26]
		oneRecord.RindTypeEn = each[27]
		oneRecord.RindTypeFr = each[28]
		oneRecord.LastUpdateDate = each[29]

		allRecords = append(allRecords, oneRecord)
	}

	jsonData, err := json.MarshalIndent(allRecords, "", "    ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Writing ./CanadianCheeseDirectory.json...")

	jsonFile, err := os.Create("./CanadianCheeseDirectory.json")
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	jsonFile.Write(jsonData)

}

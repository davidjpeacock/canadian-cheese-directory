# Canadian Cheese Directory to JSON

[![Build Status](https://travis-ci.org/davidjpeacock/canadian-cheese-directory.svg?branch=master)](https://travis-ci.org/davidjpeacock/canadian-cheese-directory)

Parser for Canadian Cheese Directory Open Data.  The Canadian Cheese Directory is kindly provided by the Canada Open Data Portal, but unfortunately, only in CSV or text format.

This tool is provided to grab the latest data and output it in beautiful, workable, and human readable JSON.

## Installation

```
go get github.com/davidjpeacock/canadian-cheese-directory
```

## Usage

```
$GOPATH/bin/canadian-cheese-directory
```

This will output the Canadian Cheese Directory data to a file in your current working directory, in a nicely indented JSON format.

## License

 MIT
 
## Contributing

Pull requests welcome.
